function loading() {
	console.log("loading");
	if (document.cookie=="") {
		console.log("no cookie exists, setting light mode");
		document.cookie="mode=light;SameSite=Lax";
	}
	if (document.cookie=="mode=dark") {
		console.log("dark mode cookie exists, setting dark mode");
		document.getElementById('style').setAttribute('href',"style-dark.css");
		document.getElementById('click').setAttribute('src',"/light.svg");
	}
}

function swapCSS(){
	if (document.cookie=="mode=light") {
		console.log("light mode cookie exists, setting dark mode");
		document.getElementById('style').setAttribute('href', "style-dark.css");
		document.cookie = "mode=dark;SameSite=Lax";
		document.getElementById('click').setAttribute('src',"/light.svg");
	} else {
		console.log("dark mode cookie exists, setting light mode");
		document.getElementById('style').setAttribute('href', "style.css");
		document.cookie = "mode=light;SameSite=Lax";
		document.getElementById('click').setAttribute('src',"/dark.svg");
	}
}

loading();
